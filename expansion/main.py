import mqtt
import machine
import time
import json

from Eduponics import mcp23017,ads1x15
from machine import I2C,ADC,Pin

power = Pin(12, Pin.OUT)
power.value(1)
time.sleep(0.1)
# define i2c connection to the extension board
i2c = I2C(scl=Pin(33), sda=Pin(32))

# setup adc for the extension board (default address for MCP might be 0x20)
ads_address = 0x48
mcp_address = 0x27
gain = 1

adc = ads1x15.ADS1115(i2c=i2c, address=ads_address,mcp_address=mcp_address, gain=gain)
#adc = ADC(Pin(35))

discovery_topic_prefix = "homeassistant/sensor"
discovery_config_template = { "value_template": "{{ value | round(4) * 100 }}",
                              "unit_of_measurement": "%" }

state_topic_prefix = "esp32/agriculture/"

discovery_configs = []

calibration = ((350, 4096,"enclosed_long"),
               (380, 26064,"open_long"),
               (380, 26176,"open_short"))

plants = [("avocado3",0,0),
          ("avocado2",1,0),
          ("avocado3",2,1),
          ("lemon",3,2)]
    
for plant in plants:
    config = discovery_config_template
    config["name"] = plant[0]
    config["state_topic"] = state_topic_prefix + plant[0] + "/moisture_pct"
    config["unique_id"] = plant[0] + "_uniq"
    discovery_configs.append(config)

def on_message_callback(topic, msg):
    '''
    get the message and topic and print it
    '''
    print((topic, msg))
    #pass

def connect_and_subscribe():

    print("[-] Connecting to MQTT client ...")
    # set the MQTT broker object
    client = mqtt.MQTTClient()
    # set a callback for incoming messages (subscribed topics)
    client.set_callback(on_message_callback)
    # connect to the broker
    client.connect()

    # subscribe to the topics
    #for topic in topics:
    #    client.subscribe("%s" % topic)
    #    print("[-] Subscribed to %s successfully" % topic)
    #    print("[-] Connected to %s MQTT broker successfully" % client.server)

    return client

def restart_and_reconnect():
    # something  went wrong, reconnect in 5 seconds ...
    print('[-] Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(5)
    machine.reset()

try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()

# configure few variables
last_message = 0
message_interval = 1

last_discovery = 0
discovery_interval = 30

while True:
    try:
        # check if there are new messages pending to be processed
        # if there are, redirect them to callback on_message_callback()
        client.check_msg()
        
        if (time.time() - last_discovery) > discovery_interval:
            last_discovery = time.time()
            
            for config in discovery_configs:
                client.publish("%s/%s/config" % (discovery_topic_prefix, config["name"]), json.dumps(config))
                print("[-] published %s discovery config" % config["name"])

        # check if the last published data wasn't less than message_interval
        if (time.time() - last_message) > message_interval:
            for plant in plants:
                adc_read = adc.read(plant[1])
                
                voltage = adc_read["voltage"]
                value = adc_read["raw"]
                
                #adc.read()
                #adc.atten(ADC.ATTN_11DB)
                #value = adc.read()
                
                minVal = calibration[plant[2]][0]
                maxVal = calibration[plant[2]][1]
                
                moisture = 1.0 - (value - minVal) / (maxVal - minVal)
                
                client.publish("esp32/agriculture/%s/moisture_pct" % plant[0], str(moisture))
                client.publish("esp32/agriculture/%s/moisture_raw" % plant[0], str(value))
                print("[-] published %s moisture status: %s" % (plant[0], str(moisture)))
                time.sleep(0.5)

            # update last message timestamp
            last_message = time.time()

    except OSError as e:
        # if something goes wrong, reconnect to MQTT server
        restart_and_reconnect()


