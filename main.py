import mqtt
import machine
import time
import json
import uhashlib
import ubinascii
import urequests

from constants import DEVICE_NAME,MIN_VAL,MAX_VAL
from machine import I2C,ADC,Pin

adc = ADC(Pin(35))
pump = Pin(23, Pin.OUT)
pump.value(0)

macaddr = station.config('mac')

sensor_hashed_id = uhashlib.sha256(macaddr + "sensor")
switch_hashed_id = uhashlib.sha256(macaddr + "switch")
update_hashed_id = uhashlib.sha256(macaddr + "update")

sensor_unique_id = ubinascii.hexlify(sensor_hashed_id.digest())
switch_unique_id = ubinascii.hexlify(switch_hashed_id.digest())
update_unique_id = ubinascii.hexlify(update_hashed_id.digest())

discovery_topic_prefix = "homeassistant"

state_topic_prefix = "esp32/agriculture/"

discovery_sensor_config = {}
discovery_sensor_config["name"] = DEVICE_NAME + "_sensor"
discovery_sensor_config["state_topic"] = state_topic_prefix + DEVICE_NAME + "/moisture_pct"
discovery_sensor_config["unique_id"] = sensor_unique_id
discovery_sensor_config["value_template"] = "{{ value }}"
discovery_sensor_config["unit_of_measurement"] = "%"

discovery_switch_config = {}
discovery_switch_config["name"] = DEVICE_NAME + "_switch"
discovery_switch_config["state_topic"] = state_topic_prefix + DEVICE_NAME + "/pump"
discovery_switch_config["command_topic"] = state_topic_prefix + DEVICE_NAME + "/set"
discovery_switch_config["unique_id"] = switch_unique_id

discovery_update_switch_config = {}
discovery_update_switch_config["name"] = DEVICE_NAME + "_update"
discovery_update_switch_config["state_topic"] = state_topic_prefix + DEVICE_NAME + "/update"
discovery_update_switch_config["command_topic"] = state_topic_prefix + DEVICE_NAME + "/update"
discovery_update_switch_config["unique_id"] = update_unique_id

pump_activated_at = 0
pump_run_interval = 30

update_requested = False
updating = False

def update_firmware():
    client.publish("esp32/agriculture/%s/update" % DEVICE_NAME, "OFF")
    updating = True

    gc.collect()
    print("[-] Updating Firmware")

    t = urequests.get(constants.UPDATE_URL).text
    f = open('main.py', 'w')
    f.write(t)
    f.close()
    
    print("[-] Restarting")
    restart()
        
def activate_pump():
    print("[-] Activating Pump")
    global pump_activated_at
    pump_activated_at = time.time()
    pump.value(1)
    client.publish("esp32/agriculture/%s/pump" % DEVICE_NAME, "ON")

def deactivate_pump():
    print("[-] Deactivating Pump")
    pump.value(0)
    global pump_activated_at
    pump_activated_at = 0
    client.publish("esp32/agriculture/%s/pump" % DEVICE_NAME, "OFF")

def on_message_callback(topic, msg):
    global update_requested
    
    if topic.decode() == discovery_switch_config["command_topic"] and msg.decode().lower() == "on":
        activate_pump()
    elif topic.decode() == discovery_switch_config["command_topic"] and msg.decode().lower() == "off":
        deactivate_pump()
    elif topic.decode() == discovery_update_switch_config["command_topic"] and msg.decode().lower() == "on":
        update_requested = True

def connect_and_subscribe():

    print("[-] Connecting to MQTT client ...")
    client = mqtt.MQTTClient()
    client.set_callback(on_message_callback)
    client.connect()

    client.subscribe("%s" % discovery_switch_config["command_topic"])
    print("[-] Subscribed to %s successfully" % discovery_switch_config["command_topic"])

    client.subscribe("%s" % discovery_update_switch_config["command_topic"])
    print("[-] Subscribed to %s successfully" % discovery_update_switch_config["command_topic"])
    
    print("[-] Connected to %s MQTT broker successfully" % client.server)
    
    return client

def restart():
    time.sleep(5)
    machine.reset()

try:
    client = connect_and_subscribe()
except OSError as e:
    print('[-] Failed to connect to MQTT broker. Reconnecting...')
    restart()

last_message = 0
message_interval = 5

last_discovery = 0
discovery_interval = 30

while True:
    try:
        if update_requested:
            update_firmware()
        elif not updating:
            client.check_msg()

            if pump_activated_at > 0 and (time.time() - pump_activated_at) > pump_run_interval:
                deactivate_pump()
            
            if (time.time() - last_discovery) > discovery_interval:
                last_discovery = time.time()
            
                client.publish("%s/sensor/%s/config" % (discovery_topic_prefix,
                                                        discovery_sensor_config["name"]),
                               json.dumps(discovery_sensor_config))
                client.publish("%s/switch/%s/config" % (discovery_topic_prefix,
                                                        discovery_switch_config["name"]),
                               json.dumps(discovery_switch_config))
                client.publish("%s/switch/%s/config" % (discovery_topic_prefix,
                                                        discovery_update_switch_config["name"]),
                               json.dumps(discovery_update_switch_config))
                
                print("[-] published discovery configs")
                
            if (time.time() - last_message) > message_interval:
                adc.read()
                adc.atten(ADC.ATTN_11DB)
                value = adc.read()
                
                moisture = 1.0 - (value - MIN_VAL) / (MAX_VAL - MIN_VAL)

                moisture_fmt = "{:.2f}".format(moisture * 100)
                client.publish("esp32/agriculture/%s/moisture_pct" % DEVICE_NAME, moisture_fmt)
                client.publish("esp32/agriculture/%s/moisture_raw" % DEVICE_NAME, str(value))
                print("[-] published %s moisture status: %s" % (DEVICE_NAME, moisture_fmt))
                
                last_message = time.time()

    except OSError as e:
        print('[-] Failed to connect to MQTT broker. Reconnecting...')
        restart()


