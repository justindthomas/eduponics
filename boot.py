import network
import esp
esp.osdebug(None)
import gc
gc.collect()
import constants

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(constants.WLAN_SSID,constants.WLAN_PASSPHRASE)

while station.isconnected() == False:
  pass

print('Connected to WiFi successfully, IP: %s' % station.ifconfig()[0])
